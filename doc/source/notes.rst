.. include:: substitutions.txt

.. _Notes:

=====
Notes
=====

.. note:: The packages on this site are maintained by the `Debian Tryton Maintainers`_, but nevertheless they
        are unofficial and you are using them at your own risk (as always).

.. _Debian Tryton Maintainers: https://qa.debian.org/developer.php?login=maintainers%40debian.tryton.org

.. IMPORTANT::
   Effective from October 2019 the DNS record for debian.tryton.org was removed.

   The mirror has been moved to

     * https://tryton.debian.net/debian/ (http only)

     * https://debian.m9s.biz/debian/ (http and https)

   Please update your sources lists accordingly, depending on your preference for http or https.

.. note:: You will still encounter debian.tryton.org as origin description. It was kept for compatibility
        reasons to avoid breaking existent installations.

.. note:: Packages for Debian jessie are deprecated since 2019-04-03, they can no more be built due to
        missing dependencies provided by deprecated jessie-backports. Please upgrade to a more recent release.


