.. _Tryton Debian Supporters:

========================
Tryton Debian Supporters
========================

debian.tryton.org is initiated and brought to you by `MBSolutions`_, the maintenance being done by 
Debian Developer `Mathias Behrle`_.

This service depends on sponsoring and is currently mainly helped by the sponsoring  of
`Freexian`_ to whom our special thanks go.


Supporters List
===============

in alphabetical order

`Entr'ouvert`_

`Freexian`_

`Hanseatische Gebäudetechnik`_

`MBSolutions`_

.. _Mathias Behrle: https://qa.debian.org/developer.php?login=mbehrle%40debian.org
.. _Entr'ouvert: https://www.entrouvert.com/
.. _Freexian: https://www.freexian.com/
.. _MBSolutions: http://www.m9s.biz/de-de/article/debian 
.. _Hanseatische Gebäudetechnik: http://www.hanseatische-gebaeudetechnik.de/

