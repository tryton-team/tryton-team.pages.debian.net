.. include:: substitutions.txt

.. _Workflow:

==================
Packaging Workflow
==================


Basically the packages of the Tryton team adhere to same policy as the
`Debian Python Team`_ and follow the `Policy requirements for packaged Python programs and modules`_.


.. _Debian Python Team: https://salsa.debian.org/python-team/tools/python-modules/blob/master/policy.rst
.. _Policy requirements for packaged Python programs and modules: https://www.debian.org/doc/packaging-manuals/python-policy/


Tryton team policy
==================

There are some exceptions to observe with Tryton packages. The branching model is slightly different mainly to enable the tracking
of different Tryton series per Debian release.

Git repositories live on Salsa under the url git@salsa.debian.org:tryon-team/<src-pkg-name>.git. The typical Git layout of a Tryton repos could look like

::

  * debian
    contains the top version of the debianized repos

    debian-bullseye
    contains the last state of the named Debian release (this branch is forked from the series branch whenever needed for security or execeptional bug fix releases)

    until Tryton series 6.0: debian-bullseye-6.0
    series branch: contains the last state of the debianized repos following the Tryton bug fix releases as released from the Tryton project (the naming follows debian-<Debian Release first entered>-<Tryton-series>)

    starting from Tryton series 7.0: debian-7.0
    series branch: contains the last state of the debianized repos following the Tryton bug fix releases as released from the Tryton project (the naming follows debian-<Tryton-series>)
  
  * upstream
    contains the top version of the upstream package code

    upstream-6.0
    series branch: contains the last state of the upstream package code following the Tryton bug fix releases as released from the Tryton project (the naming follows debian-<Tryton-series>)

  * pristine-tar
    contains the orig tarball diffs and signatures generated with pristine-tar


.. IMPORTANT::
   When importing new upstream tarballs the according upstream branches MUST be renamed beforehand and named back later. E.g. when importing for the Tryton series 6.0 this would be

   - git branch -m upstream upstream.bak
   - git branch -m upstream-6.0 upstream

   Do the import of the tarball into upstream and the pristine-tar import

   When you are done

   - git branch -m upstream upstream-6.0
   - git branch -m upstream.bak upstream

   The reason for this procedure lies in the nature of pristine-tar producing deltas against the upstream branch. Those deltas would be bloated when based on different series.


- Patches are maintained with quilt

- Salsa CI is activated for all repos of the Tryton Team.



New package
===========

A special flavor of Pypi2deb is available for Tryton. The quickest and simplest way to create a Debian package for a module is


::

  $ sudo apt install pypi2deb (to satisfy dependencies)
  $ git clone https://salsa.debian.org/tryton-team/tools/pypi2deb.git -b tryton
  $ ./pypi2deb/py2dsp --profile tryton --series 7.0 --tryton trytond_<module_name>

This will result in a complete Tryton module package with base branching done in a git repository.
The git repository will automatically be created and pushed to salsa.debian.org (provided the user is a member of tryton-team).

After the creation still special care should be taken to

  - review the Depends (pypi2deb uses the algorithms of dh-python to guess dependencies and can therefore only know about packages already part of Debian.
    For unknown packages Depends have to be filled in manually.)

  - review the package long description, which is parsed from upstream sphinx documentation

  - review the autopkgtest Depends (they must contain the test_requires defined in setup.py *and* the extras_depend defined in tryton.conf)

  - run a test build with lintian check



Tools
=====

While it is perfectly possible to manage Tryton Repositories with `git-buildpackage`_ there exists also `git-stuff`_ as a different tool set allowing for more fine-grained and a bit different approach to control the import/commit/release steps.


::

  $ git clone https://salsa.debian.org/tryton-team/tools/git-stuff.git
  
For further use refer to the included documentation.


.. _git-buildpackage: https://git.sigxcpu.org/cgit/git-buildpackage/
.. _git-stuff: https://salsa.debian.org/tryton-team/tools/git-stuff/-/tree/master
