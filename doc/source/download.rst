.. include:: substitutions.txt

.. _Download:

========
Download
========

The download URLs are indicated on the summary of each package on salsa.debian.org.

Anonymous download
==================

Via https protcol

::

  $ git clone https://salsa.debian.org/tryton-team/<package>.git


Authenticated download (Developers with push access)
====================================================


With ssh key registered on the server

::

  $ git clone git@salsa.debian.org:tryton-team/<package>.git




Download of the complete tree
=============================

To manage the access to salsa.debian.org install the salsa package

::

  $ sudo apt install salsa


A download of the complete tree of the Tryton team is possible with

::

  $ salsa --group tryton-team co --all



