.. include:: substitutions.txt

==========================
Tryton Packages for Debian
==========================

Apt Package Repository (Unofficial)
-----------------------------------

.. toctree::
   :maxdepth: 2

   mirror
   
   gnuhealth

   notes

Browse the apt mirror:

   `Tryton Apt Repository via debian.net (http only)`_
   
   `Tryton Apt Repository via m9s.biz (https)`_

.. _Tryton Apt Repository via debian.net (http only): http://tryton.debian.net/debian/
.. _Tryton Apt Repository via m9s.biz (https): https://debian.m9s.biz/debian/

Development
-----------

.. toctree::
   :maxdepth: 2

   supporters

   mailinglists
   
   download
   
   packaging_workflow

   links

Browse the git repositories on salsa.debian.org:

   `Git Repositories`_

.. _Git Repositories: https://salsa.debian.org/tryton-team 

Indices and tables
------------------

* :ref:`search`

