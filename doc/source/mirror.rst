.. include:: substitutions.txt

.. _Tryton Debian Apt Repository:

============================
Tryton Debian Apt Repository
============================

Why tryton.debian.net?
======================

Due to the different release schemata and policies of Tryton and Debian it is
currently impossible to make all Tryton releases available for the different
Debian suites in the main repository of Debian.

Our intention is to make available Tryton releases including their bug fix releases
via this aptable package mirror.

By using this mirror

* you can run the Tryton release of your choice on your current stable or testing Debian system (oldstable supported as long as possible).
* you are able to upgrade to the next Tryton series as soon as `you` wish to do so.
* you are independent from the next Tryton major entering in Debian main.
* you get all bug fix releases for all supported Tryton series.

.. IMPORTANT::
   This service depends on sponsoring, a full list of current and former supporters can be found on the
   :doc:`supporters` page.

   Please note that the current ressources allow for:
     * Packaging of Tryton LTS series with inclusion in Debian main
     * Immediate packaging of Tryton security releases depending on their impact
     * Packaging of the first 4 Tryton (LTS) bugfix releases after the release
     * Packaging of Tryton bugfix releases for the supported series in a quarterly rhythm (i.e. all 3 months)

   If you want to help or if you need to get more Tryton series, more releases or specific packages maintained
   you are very welcome to get in touch with the `maintainers`_.

.. _maintainers: mailto:mbehrle@debian.org



Scope
=====

The general scope provided by Debian Tryton Maintainers is

* Released series of the `Tryton Project`_.
* Some additional dependencies needed by the Tryton packages.
* Released series of the `GNU Health Project`_.

.. _Tryton Project: http://www.tryton.org/
.. _GNU Health Project: http://health.gnu.org/

How To
======

To use this package mirror with the apt package mangement of your Debian system you must 
make available the according sources.list to your system. The following steps have to be performed:

- Import the signing key of the mirror.

- Import the suitable sources.list for your target Debian release and Tryton series.

- Tell the system to take Tryton related packages preferrably from this mirror.

Please read below for further instructions on how to do that.

For the installation of GNU Health you find additional instructions at :doc:`gnuhealth`.

For the server configuration please refer to /usr/share/doc/tryton-server/README.Debian on your 
Debian system or look at the `latest version of README.Debian in the VCS`_.

.. _latest version of README.Debian in the VCS: https://salsa.debian.org/tryton-team/tryton-server/blob/debian/debian/tryton-server.README.Debian

Supported Distributions
-----------------------

Naming convention for distributions is <Debian-Release>-<Tryton-Version>

e.g. buster-5.0, bullseye-6.0

Packages suitable for testing and unstable are available under the distribution <Debian-Future-Release>-<Tryton-Version>

e.g. at the time of writing bookworm-6.0

All available distributions can be found at `Debian Tryton Distributions`_.

.. _Debian Tryton Distributions: http://debian.m9s.biz/debian/dists/


Mirror Key and Sources
----------------------

First it is required to add the signature key of this mirror to apt

::

 $ sudo curl -o /etc/apt/trusted.gpg.d/debian.tryton.org-archive.gpg http://debian.m9s.biz/debian/debian.tryton.org-archive.gpg


Adding the sources to apt basically works like

::

 $ echo "deb https://debian.m9s.biz/debian/ <distribution> main" | sudo tee -a /etc/apt/sources.list

The following chapter provides ready-to-use examples for the ditribution and series of your choice.


Ready-to-use
=============

The following examples show the download of ready-to-use source list files. They can be downloaded
and saved directly to /etc/apt/sources.list.d by running the command of your target distribution and series

The list below contains only the entries for the actual supported Tryton releases. If you are running a
Tryton version not listed below you should consider upgrading to a supported version.

The complete list of former and actual distributions can be found at https://debian.m9s.biz/debian/


*Distribution buster-6.0*

::

 $ FILE=tryton-buster-6.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE


*Distribution bullseye-6.0*

::

 $ FILE=tryton-bullseye-6.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE

*Distribution bullseye-7.0*

::

 $ FILE=tryton-bullseye-7.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE

*Distribution bookworm-6.0*

::

 $ FILE=tryton-bookworm-6.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE

*Distribution bookworm-7.0*

::

 $ FILE=tryton-bookworm-7.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE


*Distribution trixie-6.0*

::

 $ FILE=tryton-trixie-6.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE

*Distribution trixie-7.0*

::

 $ FILE=tryton-trixie-7.0.list; sudo curl -o /etc/apt/sources.list.d/$FILE http://debian.m9s.biz/debian/$FILE



Installation
============

Update the package lists

::

 $ sudo apt update

 
Choose between the following options for your use case:

Install just the server

::

 $ sudo apt install tryton-server [-t <distribution>]

 
Install all modules incl. server

::

 $ sudo apt install tryton-modules-all [-t <distribution>]


Install a complete production ready Tryton server environment including the guided setup of a wsgi backend (uWSGI), a web frontend (nginx) with optionally setup of secure access via https with Letsencrypt certificates

::

 $ sudo apt install tryton-server-all-in-one [-t <distribution>]


Install the client to access the server from your local or remote machine

::

 $ sudo apt install tryton-client [-t <distribution>]



Pinning
=======

The versioning of Tryton packages from debian.tryton.org is conservative with respect to upgrades. It follows
the ususal backports schema to provide upgradibility to a new Debian release at every moment. Therefore it may be, 
that apt will want to replace the installed version with a *newer* version number, that is evtl. available
from other (or the default) sources.lists.

To stick with the packages of debian.tryton.org, it is favorable to pin the packages. With pin priority >1000 they will 
always be preferred over other packages. A sample looks like

::

  Package: *
  Pin: release o=debian.tryton.org
  Pin-Priority: 999


A suitable file can be downloaded with

::

 $ sudo curl -o /etc/apt/preferences.d/debian.tryton.org.pref http://debian.m9s.biz/debian/debian.tryton.org.pref
 
 
Use the new preferences with

::

 $ sudo apt update && apt dist-upgrade

